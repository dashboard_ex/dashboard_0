
let btn_menu = document.getElementById("btn-menu");
let side_bar = document.getElementsByClassName("side-bar").item(0);
let content_main = document.getElementsByClassName("content-main").item(0);

function sideBarMove() {
	console.log(side_bar.style.left);
	if(side_bar.style.left === "-" + side_bar.offsetWidth + "px"){
		side_bar.style.left = 0 + "px";
		content_main.style.left =  side_bar.offsetWidth + "px";
		content_main.style.width = window.innerWidth - side_bar.offsetWidth + "px";
	} else {
		side_bar.style.left = -side_bar.offsetWidth + "px";
		content_main.style.left =  0 + "px";
		content_main.style.width = window.innerWidth + "px";

	}
}


var acc = document.getElementsByClassName("side-bar-item-drop-button");

for (let i = 0; i < acc.length; i++) {
	acc[i].addEventListener("click", function() {
		this.classList.toggle("active");
		var panel = this.nextElementSibling;
		if (panel.style.maxHeight){
			panel.style.maxHeight = null;
		} else {
			panel.style.maxHeight = panel.scrollHeight + "px";
		}
	});
}

var acp = document.getElementsByClassName("side-bar-item");

for (let i = 0; i < acp.length; i++) {
	acp[i].addEventListener("click", function() {
		for (let i = 0; i < acp.length; i++) { acp[i].classList.remove("active"); }
		this.classList.toggle("active");
	});
}